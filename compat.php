<?php
class WpJukebox_Compatibility {

	/**
	 * Minimun version on PHP require to activate the plugin
	 *
	 * @var string
	 *
	 * @since 0.1
	 */
	const PHP_MIN_VERSION = '5.4';

	/**
	 * admin_init hook callback
	 *
	 * @since 0.1
	 */
	public static function admin_init() {

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			return;
		}

		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}

		load_plugin_textdomain( 'wpjukebox', false, plugin_dir_path( __FILE__ ) . 'languages' );

		trigger_error( sprintf( __( 'Plugin Boilerplate requires PHP version %s or greater to be activated.', 'plugin-boilerplate' ), PluginBoilerplate_Compatibility::PHP_MIN_VERSION ) );

		deactivate_plugins( dirname( plugin_basename( __FILE__ ) ) . '/wp-jukebox.php' );

		unset( $_GET['activate'] );

		add_action( 'admin_notices', array( 'WpJukebox_Compatibility', 'admin_notice' ) );
	}

	/**
	 * Notify the user about the incompatibility issue.
	 */
	public static function admin_notice() {
		echo '<div class="notice error is-dismissible">';
		echo '<p>' . esc_html( sprintf( __( 'WP Jukebox require PHP version %s or greater to be activated. Your server is currently running PHP version %s.', 'wpjukebox' ), PluginBoilerplate_Compatibility::PHP_MIN_VERSION, PHP_VERSION ) ) . '</p>';
		echo '</div>';
	}
}