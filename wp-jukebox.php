<?php
/*
Plugin Name: WP Jukebox
Description: Easily manage your music collection.
Author: PetitPHP
Author URI: https://github.com/petitphp
Version: 0.1
Text Domain: wpjukebox
 */

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// plugin requires PHP 5.4 or greater
if ( version_compare( PHP_VERSION, '5.4.0', '<' ) ) {
	if ( ! class_exists( 'WpJukebox_Compatibility' ) ) {
		require_once( dirname(__FILE__) . '/compat.php' );
	}
	// possibly display a notice, trigger error
	add_action( 'admin_init', array( 'WpJukebox_Compatibility', 'admin_init' ) );

	// stop execution of this file
	return;
}

require_once( dirname(__FILE__) . '/autoload.php' );

/**
 * Bootstrap WP Jukebox
 */
add_action( 'plugins_loaded', array( '\WpJukebox\Plugin', 'init' ) );
