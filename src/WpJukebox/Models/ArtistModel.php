<?php
namespace WpJukebox\Models;

class ArtistModel {

	/**
	 * artist's id
	 *
	 * @var int
	 */
	protected $id;

	/**
	 * base wp_post
	 *
	 * @var \WP_Post
	 */
	protected $_post;

	/**
	 * artist's name
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * artist birthdate
	 *
	 * @var \DateTime
	 */
	protected $birthdate;

	/**
	 * Ctor
	 *
	 * @param int|\WP_Post$ artist
	 */
	public function __construct( $artist ) {

	}
}