<?php
namespace WpJukebox\Admin;

/**
 * WP Jukebox admin
 *
 * @package WpJukebox\Admin
 *
 * @since 0.1
 */
class Admin {

	/**
	 * Load WP Jukebox admin
	 *
	 * @since 0.1
	 */
	public static function init() {

		add_action( 'add_meta_boxes', array( __CLASS__, 'add_tracks_meta_boxes' ) );
	}

	/**
	 * Add tracks metabox to the album edit page
	 *
	 * @since 0.1
	 */
	public static function add_tracks_meta_boxes() {

		add_meta_box(
			'myplugin_sectionid',
			__( 'Album tracklist', \WpJukebox\Plugin::TEXT_DOMAIN ),
			array( __CLASS__, 'tracks_metabox_callback' ),
			WPJBX_ALBUM
		);
	}

	/**
	 * Display the tracks metabox
	 *
	 * @param \WP_Post $post current edited post
	 *
	 * @since 0.1
	 */
	public static function tracks_metabox_callback( $post ) {

		$template = WPJBX_VIEWS_DIR . 'admin/tracks-metabox.php';
		if( file_exists( $template ) ) {
			include_once( $template );
		} else {
			echo '<p>Missing template file.</p>';
		}
	}
}