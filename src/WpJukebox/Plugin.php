<?php
namespace WpJukebox;

/**
 * WP Jukebox plugin
 *
 * @package WpJukebox
 *
 * @since 0.1
 */
class Plugin {

	/**
	 * Plugin's version
	 *
	 * @var string
	 *
	 * @since 0.1
	 */
	const VERSION = '0.1';

	/**
	 * Text domain for translation
	 *
	 * @var string
	 *
	 * @since 0.1
	 */
	const TEXT_DOMAIN = 'wpjukebox';

	/**
	 * Bootstrap WP Jukebox plugin
	 *
	 * @since 0.1
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'load_textdomain' ) );

		add_action( 'init', array( __CLASS__, 'register_artist_ptype' ) );
		add_action( 'init', array( __CLASS__, 'register_album_ptype' ) );
		add_action( 'init', array( __CLASS__, 'register_track_ptype' ) );
		add_action( 'init', array( __CLASS__, 'register_genre_taxonomy' ) );

		if ( is_admin() ) {
			add_action( 'admin_init', array( '\WpJukebox\Admin\Admin', 'init' ) );
		}
	}

	/**
	 * Load plugin's text domain
	 *
	 * @since 0.1
	 */
	public static function load_textdomain() {
		load_textdomain( static::TEXT_DOMAIN, WPJBX_DIR . '/languages' );
	}

	/**
	 * Register artist post type
	 *
	 * @since 0.1
	 */
	public static function register_artist_ptype() {
		$labels = array(
			'name'               => _x( 'Artists', 'post type general name', static::TEXT_DOMAIN ),
			'singular_name'      => _x( 'Artist', 'post type singular name', static::TEXT_DOMAIN ),
			'menu_name'          => _x( 'Artists', 'admin menu', static::TEXT_DOMAIN ),
			'name_admin_bar'     => _x( 'Artist', 'add new on admin bar', static::TEXT_DOMAIN ),
			'add_new'            => _x( 'Add New', 'artist', static::TEXT_DOMAIN ),
			'add_new_item'       => __( 'Add New Artist', static::TEXT_DOMAIN ),
			'new_item'           => __( 'New Artist', static::TEXT_DOMAIN ),
			'edit_item'          => __( 'Edit Artist', static::TEXT_DOMAIN ),
			'view_item'          => __( 'View Artist', static::TEXT_DOMAIN ),
			'all_items'          => __( 'All Artists', static::TEXT_DOMAIN ),
			'search_items'       => __( 'Search Artists', static::TEXT_DOMAIN ),
			'parent_item_colon'  => __( 'Parent Artists:', static::TEXT_DOMAIN ),
			'not_found'          => __( 'No artists found.', static::TEXT_DOMAIN ),
			'not_found_in_trash' => __( 'No artists found in Trash.', static::TEXT_DOMAIN )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => WPJBX_ARTIST ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
		);

		register_post_type( WPJBX_ARTIST, $args );
	}

	/**
	 * Register album post type
	 *
	 * @since 0.1
	 */
	public static function register_album_ptype() {
		$labels = array(
			'name'               => _x( 'Albums', 'post type general name', static::TEXT_DOMAIN ),
			'singular_name'      => _x( 'Album', 'post type singular name', static::TEXT_DOMAIN ),
			'menu_name'          => _x( 'Albums', 'admin menu', static::TEXT_DOMAIN ),
			'name_admin_bar'     => _x( 'Album', 'add new on admin bar', static::TEXT_DOMAIN ),
			'add_new'            => _x( 'Add New', 'album', static::TEXT_DOMAIN ),
			'add_new_item'       => __( 'Add New Album', static::TEXT_DOMAIN ),
			'new_item'           => __( 'New Album', static::TEXT_DOMAIN ),
			'edit_item'          => __( 'Edit Album', static::TEXT_DOMAIN ),
			'view_item'          => __( 'View Album', static::TEXT_DOMAIN ),
			'all_items'          => __( 'All Albums', static::TEXT_DOMAIN ),
			'search_items'       => __( 'Search Albums', static::TEXT_DOMAIN ),
			'parent_item_colon'  => __( 'Parent Albums:', static::TEXT_DOMAIN ),
			'not_found'          => __( 'No albums found.', static::TEXT_DOMAIN ),
			'not_found_in_trash' => __( 'No albums found in Trash.', static::TEXT_DOMAIN )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => WPJBX_ALBUM ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		);

		register_post_type( WPJBX_ALBUM, $args );
	}

	/**
	 * Register track post type
	 *
	 * @since 0.1
	 */
	public static function register_track_ptype() {
		$labels = array(
			'name'               => _x( 'Tracks', 'post type general name', static::TEXT_DOMAIN ),
			'singular_name'      => _x( 'Track', 'post type singular name', static::TEXT_DOMAIN ),
			'menu_name'          => _x( 'Tracks', 'admin menu', static::TEXT_DOMAIN ),
			'name_admin_bar'     => _x( 'Track', 'add new on admin bar', static::TEXT_DOMAIN ),
			'add_new'            => _x( 'Add New', 'album', static::TEXT_DOMAIN ),
			'add_new_item'       => __( 'Add New Track', static::TEXT_DOMAIN ),
			'new_item'           => __( 'New Track', static::TEXT_DOMAIN ),
			'edit_item'          => __( 'Edit Track', static::TEXT_DOMAIN ),
			'view_item'          => __( 'View Track', static::TEXT_DOMAIN ),
			'all_items'          => __( 'All Tracks', static::TEXT_DOMAIN ),
			'search_items'       => __( 'Search Tracks', static::TEXT_DOMAIN ),
			'parent_item_colon'  => __( 'Parent Tracks:', static::TEXT_DOMAIN ),
			'not_found'          => __( 'No tracks found.', static::TEXT_DOMAIN ),
			'not_found_in_trash' => __( 'No tracks found in Trash.', static::TEXT_DOMAIN )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => WPJBX_TRACK ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
		);

		register_post_type( WPJBX_TRACK, $args );
	}

	/**
	 * Register genre taxonomy
	 *
	 * @since 0.1
	 */
	public static function register_genre_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Genres', 'taxonomy general name' ),
			'singular_name'              => _x( 'Genre', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Genres' ),
			'popular_items'              => __( 'Popular Genres' ),
			'all_items'                  => __( 'All Genres' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Genre' ),
			'update_item'                => __( 'Update Genre' ),
			'add_new_item'               => __( 'Add New Genre' ),
			'new_item_name'              => __( 'New Genre Name' ),
			'separate_items_with_commas' => __( 'Separate genres with commas' ),
			'add_or_remove_items'        => __( 'Add or remove genres' ),
			'choose_from_most_used'      => __( 'Choose from the most used genres' ),
			'not_found'                  => __( 'No genres found.' ),
			'menu_name'                  => __( 'Genres' ),
		);

		$args = array(
			'hierarchical'          => false,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => WPJBX_MUSIC_GENRE ),
		);

		register_taxonomy( WPJBX_MUSIC_GENRE, WPJBX_ALBUM, $args );
	}
}