<?php

// Plugin constants
define( 'WPJBX_VERSION', '0.1' );
define( 'WPJBX_URL', plugin_dir_url( __FILE__ ) );
define( 'WPJBX_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPJBX_VIEWS_DIR', WPJBX_DIR . 'views/' );

define( 'WPJBX_ARTIST', 'artist' );
define( 'WPJBX_ALBUM', 'album' );
define( 'WPJBX_TRACK', 'track' );

define( 'WPJBX_MUSIC_GENRE', 'genre' );

/**
 * Register the autoloader for the plugin classes.
 *
 * Based off the official PSR-4 autoloader example found here:
 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
 *
 * @param string $class The class name
 *
 * @return void
 */
spl_autoload_register( function ( $class ) {
	// project-specific namespace prefix
	$prefix = 'WpJukebox\\';

	// base directory for the namespace prefix
	$base_dir = WPJBX_DIR . 'src/WpJukebox/';

	// does the class use the namespace prefix?
	$len = strlen( $prefix );
	if ( 0 !== strncmp( $prefix, $class, $len ) ) {
		// no, move to the next registered autoloader
		return;
	}

	// get the relative class name
	$relative_class = substr( $class, $len );

	// replace the namespace prefix with the base directory, replace namespace
	// separators with directory separators in the relative class name, append
	// with .php
	$file = $base_dir . str_replace( '\\', '/', $relative_class ) . '.php';

	// if the file exists, require it
	if ( file_exists( $file ) ) {
		require $file;
	}
});